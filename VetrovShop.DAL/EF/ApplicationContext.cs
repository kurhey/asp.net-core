﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using VetrovShop.Models;


namespace VetrovShop.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<User>
    {

        public ApplicationContext(DbContextOptions<ApplicationContext> options) 
            : base(options) {  }


        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Primary keys
            modelBuilder.Entity<User>().HasKey(q => q.Id);
            modelBuilder.Entity<Product>().HasKey(q => q.Id);
            modelBuilder.Entity<Order>().HasKey(q => q.Id);
            modelBuilder.Entity<ProductOrder>().HasKey(q =>
                new
                {
                    q.OrderId,
                    q.ProductId
                });

            // Relationships
            modelBuilder.Entity<ProductOrder>()
                .HasOne(t => t.Order)
                .WithMany(t => t.ProductOrders)
                .HasForeignKey(t => t.OrderId);

            modelBuilder.Entity<ProductOrder>()
                .HasOne(t => t.Product)
                .WithMany(t => t.ProductOrders)
                .HasForeignKey(t => t.ProductId);

            
        }
    }

}