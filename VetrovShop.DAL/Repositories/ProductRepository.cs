﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VetrovShop.Models;
using VetrovShop.DAL.Interfaces;
using VetrovShop.DAL.EF;

namespace VetrovShop.DAL.Repositories
{
    public class ProductRepository : IRepository<Product>
    {
        private ApplicationContext db;

        public ProductRepository(ApplicationContext context)
        {
            this.db = context;
        }

        public void Create(Product item)
        {
            db.Products.Add(item);
        }

        public void Delete(int id)
        {
            Product Product = db.Products.Find(id);

            if (Product != null)
                db.Products.Remove(Product);
        }

        public IEnumerable<Product> FindByCondition(Func<Product, bool> predicate)
        {
            return db.Products.Where(predicate).ToList();
        }

        public Product FindById(int id)
        {
            return db.Products.Find(id);
        }

        public IQueryable<Product> GetAll()
        {
            return db.Products;
        }

        public void Update(Product item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
