﻿using System;
using System.Collections.Generic;
using System.Text;
using VetrovShop.Models;


namespace VetrovShop.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }
        IRepository<ProductOrder> ProductOrders { get; }

        void Save();
    }
}

