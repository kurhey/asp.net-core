﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VetrovShop.Models
{
    public class OrderInfo
    {
        public List<string> ProductNames;
        public int OrderId;

        public OrderInfo() { ProductNames = new List<string>(); }
    }
}