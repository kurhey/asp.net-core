﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using System.Web;
//using System.Web.Mvc;
//using TaskTwo.Models;
//using Microsoft.AspNet.Identity;

//namespace TaskTwo.Controllers
//{
//    public class EditingController : Controller
//    {
//        // GET: Editing
//        [Authorize]
//        public ActionResult Index()
//        {
//            return View();
//        }

//        [HttpGet]
//        [Authorize]
//        public ActionResult EditingUser()
//        {
//            string userId = User.Identity.GetUserId();
//            using (ApplicationContext db = new ApplicationContext())
//            {
//                try
//                {
//                    if (string.IsNullOrEmpty(userId)) throw new Exception("ser ID is null ");
//                    User user = db.Users.Single(t => t.Id == userId);
//                    return View(user);
//                }
//                catch (Exception ex)
//                {
//                    System.Diagnostics.Debug.WriteLine(ex.Message);
//                    ViewBag.Error = ex.Message;
//                    return View("Error");
//                }

//            }

//        }

//        [HttpGet]
//        [Authorize]
//        public ActionResult EditingProduct(int id)
//        {
//            using (ApplicationContext db = new ApplicationContext())
//            {
//                try
//                {
//                    if (id == 0) throw new Exception("User ID is null");
//                    Product product = db.Products.Single(t => t.Id == id);
//                    return View(product);
//                }
//                catch (Exception ex)
//                {
//                    System.Diagnostics.Debug.WriteLine(ex.Message);
//                    ViewBag.Error = ex.Message;
//                    return View("Error");
//                }

//            }
//        }

//        [HttpGet]
//        [Authorize]
//        public ActionResult EditingOrder(int id = 0)
//        {
//            using (ApplicationContext db = new ApplicationContext())
//            {
//                try
//                {
//                    if (id == 0) throw new Exception("Product ID is null");
//                    Order order = db.Orders.Single(t => t.Id == id);
//                    return View(order);
//                }
//                catch (Exception ex)
//                {
//                    System.Diagnostics.Debug.WriteLine(ex.Message);
//                    ViewBag.Error = ex.Message;
//                    return View("Error");
//                }

//            }
//        }


//        [HttpPost]
//        [Authorize]
//        public ActionResult UpdUser(User user)
//        {
//            using (ApplicationContext db = new ApplicationContext())
//            {
//                try
//                {
//                    System.Diagnostics.Debug.WriteLine($"{user.Id} {user.Name}");
//                    db.Users.Attach(user);
//                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
//                    db.SaveChanges();
//                    ViewBag.Status = "Edited";
//                    return RedirectToAction("Users", "Details");
//                }

//                catch (Exception ex)
//                {
//                    System.Diagnostics.Debug.WriteLine(ex.Message);
//                    ViewBag.Error = ex.Message;
//                    return View("Error");
//                }

//            }
//        }

//        [HttpPost]
//        [Authorize]
//        public ActionResult UpdProduct(Product product)
//        {
//            using (ApplicationContext db = new ApplicationContext())
//            {
//                try
//                {
//                    System.Diagnostics.Debug.WriteLine($"{product.Id} {product.ProductName}");
//                    db.Products.Attach(product);
//                    db.Entry(product).State = System.Data.Entity.EntityState.Modified;
//                    db.SaveChanges();
//                    ViewBag.Status = "Edited";
//                    return Redirect("../Details/Products");
//                }
//                catch (Exception ex)
//                {
//                    System.Diagnostics.Debug.WriteLine(ex.Message);
//                    ViewBag.Error = ex.Message;
//                    return View("Error");
//                }

//            }
//        }

//        [HttpPost]
//        [Authorize]
//        public ActionResult UpdOrder(User user)
//        {
//            using (ApplicationContext db = new ApplicationContext())
//            {
//                try
//                {
//                    System.Diagnostics.Debug.WriteLine($"{user.Id} {user.Name}");
//                    db.Users.Attach(user);
//                    db.Entry(user).State = System.Data.Entity.EntityState.Modified;
//                    db.SaveChanges();
//                    ViewBag.Status = "Edited";
//                    return View("~/Views/Details/Orders.aspx");
//                }
//                catch (Exception ex)
//                {
//                    System.Diagnostics.Debug.WriteLine(ex.Message);
//                    ViewBag.Error = ex.Message;
//                    return View("Error");
//                }

//            }
//        }
//    }
//}