﻿using VetrovShop.Models;
using System.Net.Http;
using VetrovShop.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using VetrovShop.DAL.Identity;

namespace VetrovShop.WEB.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private static readonly HttpClient client = new HttpClient();
        private IProductService productService;


        public ProductController(IProductService service)
        {
            productService = service;
        }

        [HttpGet]
        public IActionResult Index() => View(productService.GetAll());

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create() => View("CreatingProduct");
       

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(Product product)
        {
            try
            {
                product.DateUpd = DateTime.Now;
                productService.CreateProduct(product);
                ViewBag.Status = "Product added";
                return RedirectToAction("Index", "Product");
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Update(int id) => View("UpdateProduct", productService.FindProduct(id));


        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Update(Product product)
        {
            try
            {
                productService.Update(product);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
            
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int id)
        {
            try
            {
                productService.Delete(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
            
        }
    }
}