﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VetrovShop.WEB.ViewModels;
using VetrovShop.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.Cookies;
using VetrovShop.WEB;
using System.Linq;

namespace TaskTwo.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {

        private readonly UserManager<User> _UserManager;
        private readonly SignInManager<User> _SignInManager;
        private readonly IEmailSender _emailSender;

        public AccountController(UserManager<User> _UserManager, SignInManager<User> _SignInManager, IEmailSender emailSender)
        {
            this._UserManager = _UserManager;
            this._SignInManager = _SignInManager;
            _emailSender = emailSender;
        }

        // GET: Customer
        public ActionResult Register(string returnUrl = null) 
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            IdentityResult result = null;
            ViewData["ReturnUrl"] = returnUrl;

            if(ModelState.IsValid)
            {
                User user = new User
                {
                    UserName = model.Email,
                    Name = model.Name,
                    Surname = model.Surname,
                    DateRegister = DateTime.Now,
                    Email = model.Email
                };

               

                result = await _UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {

                    var code = await _UserManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(
                        "ConfirmEmail",
                        "Account",
                        new { userId = user.Id, code = code },
                        protocol: HttpContext.Request.Scheme);

                    await _emailSender.SendEmailAsync(model.Email, "Confirmation",
                        $"Confirm your <a href='{callbackUrl}'>account</a>");

                    return Content("Для завершения регистрации проверьте электронную почту и перейдите по ссылке, указанной в письме");
                    
                }
                else
                {
                    ViewBag.Error = result.Errors;
                    return View("Error");
                }
            }
            return View(); 
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var user = await _UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                ViewBag.Error = "Confirm your account";
                return View("Error");
            }
            var result = await _UserManager.ConfirmEmailAsync(user, code);

            if(result.Succeeded)
            {
                await _SignInManager.SignInAsync(user, true);
                return RedirectToAction("Index", "Home");
            }

            return View("Error");

        }

        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _SignInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

                    if (result.Succeeded)
                    {
                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ViewBag.Status = "Incorrect username or password";
                        return View("Login");
                    }
                }
                return View(model);

            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
        }


        [HttpGet]
        public IActionResult GoogleLogin(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var provider = "Google";
            try
            {
                var redirectUrl = Url.Action(nameof(GoogleLoginCallback), "Account", new { returnUrl });
                var properties = _SignInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
                return Challenge(properties, provider);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GoogleLoginCallback(string returnUrl = null, string remoteError = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            try
            {
                string ErrorMessage = null;
                if (remoteError != null)
                {
                    ErrorMessage = $"Error from external provider: {remoteError}";
                    return RedirectToAction(nameof(Login));
                }
                var info = await _SignInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return RedirectToAction(nameof(Login));
                }


                // Sign in the user with this external login provider if the user already has a login.
                var result = await _SignInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
                if (!result.Succeeded) //user does not exist yet
                {
                    var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                    User newUser = new User
                    {
                        UserName = email,
                        Email = email,
                        EmailConfirmed = true,
                        Name = info.Principal.Identity.Name
                    };
                    var createResult = await _UserManager.CreateAsync(newUser);
                    
                    await _UserManager.AddLoginAsync(newUser, info);
                    await _UserManager.AddToRoleAsync(newUser, "Customer");
                    var newUserClaims = info.Principal.Claims.Append(new Claim("userId", newUser.Id));
                    await _UserManager.AddClaimsAsync(newUser, newUserClaims);
                    await _SignInManager.SignInAsync(newUser, isPersistent: false);
                    await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
                }

                return RedirectToLocal(returnUrl);

            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Error");
            }
            
        }

        [HttpGet]
        public async Task<ActionResult> Logout()
        {
            await _SignInManager.SignOutAsync();
            return RedirectToAction("Login");
        }


        #region Helpers
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
        #endregion

    }
}