﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VetrovShop.Services.Interfaces;
using VetrovShop.Models;
using System.Security.Claims;
using VetrovShop.WEB.ViewModels;
using System.Net.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace VetrovShop.WEB.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private IOrderService orderService;
        private IProductService productService;
        private UserManager<User> _UserManager;

        public OrderController(IOrderService orderService, IProductService productService, UserManager<User> _UserManager)
        {
            this.orderService = orderService;
            this.productService = productService;
            this._UserManager = _UserManager;
        }

        public IActionResult Index()
        {
            string userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
            List<OrderInfo> orderinfo = orderService.OrderInfo(userid); 
            return View(orderinfo);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["products"] = productService.GetAll();
            return View();
        }

        [HttpPost]
        public IActionResult CreateOrder()
        {
            string str = HttpContext.Request.Form["checkProduct"].ToString();
            string userid = User.FindFirstValue(ClaimTypes.NameIdentifier);


            int[] productId = str.Split(',').Select(t => Convert.ToInt32(t)).ToArray();

            Order order = new Order {
                DatePurchase = new DateTime(),
                UserId = userid
            };

            for (int i = 0; i < productId.Length; ++i)
            {
                var id = productId[i];
                ProductOrder po = new ProductOrder();
                po.ProductId = productId[i];
                order.ProductOrders.Add(po);
            }

            orderService.CreateOrder(order);
            return RedirectToAction("Index");
        }
    }
}