﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VetrovShop.DAL.Identity;
using VetrovShop.Models;

namespace VetrovShop.WEB.Controllers
{
    [Authorize(Roles = "Superuser")]
    public class RolesController : Controller
    {

        private RoleManager<IdentityRole> _RoleManager;
        private UserManager<User> _UserManager;

        public RolesController(RoleManager<IdentityRole> _RoleManager, UserManager<User> _UserManager)
        {
            this._RoleManager = _RoleManager;
            this._UserManager = _UserManager;
        }

        public IActionResult Index() => View(_RoleManager.Roles.ToList());

        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _RoleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _RoleManager.FindByIdAsync(id);

            if (role.Name == RoleInitializer.GetSuperRole)
                return RedirectToAction("Index");

            if (role != null)
            {
                IdentityResult result = await _RoleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }

        public IActionResult UserList() => View(_UserManager.Users.ToList());

        public async Task<IActionResult> Edit(string userId)
        {
            // получаем пользователя
            User user = await _UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _UserManager.GetRolesAsync(user);
                var allRoles = _RoleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }

            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            // получаем пользователя
            User user = await _UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _UserManager.GetRolesAsync(user);
                // получаем все роли
                var allRoles = _RoleManager.Roles.ToList();
                // получаем список ролей, которые были добавлены
                var addedRoles = roles.Except(userRoles);
                // получаем роли, которые были удалены
                var removedRoles = userRoles.Except(roles);

                await _UserManager.AddToRolesAsync(user, addedRoles);

                await _UserManager.RemoveFromRolesAsync(user, removedRoles);

                return RedirectToAction("UserList");
            }

            return NotFound();
        }
    }
}