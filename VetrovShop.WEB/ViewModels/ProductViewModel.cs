﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VetrovShop.WEB.ViewModels
{
    public class ProductViewModel
    {
        public string ProductName { get; set; }
        public int Price { get; set; }
        public string Describe { get; set; }
        public DateTime DateUpd { get; set; }
    }

    public class ProductDetailsViewModel : ProductViewModel
    {
        public int Id { get; set; }
    }

    public class ProductCreatingViewModel : ProductViewModel
    { }
}
