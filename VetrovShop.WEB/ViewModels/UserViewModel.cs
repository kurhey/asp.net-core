﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VetrovShop.WEB.ViewModels
{
    public class UserViewModel 
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

      

        public string ReturnUrl { get; set; }

    }

    public class LoginViewModel : UserViewModel
    {
       
    }

    public class RegisterViewModel : UserViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public DateTime DateRegistr { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }
    }

}
