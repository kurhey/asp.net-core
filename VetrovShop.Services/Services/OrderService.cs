﻿using System.Collections.Generic;
using System.Linq;
using VetrovShop.DAL.Interfaces;
using VetrovShop.Models;
using VetrovShop.Services.Interfaces;

namespace VetrovShop.Services
{
	public class OrderService : IOrderService
	{
        private IUnitOfWork Database { get; set; }

        public OrderService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }

        public void CreateOrder(Order order)
        {
            Database.Orders.Create(order);
            Database.Save();
        }


        public void Dispose()
        {
            Database.Dispose();
        }

        public List<OrderInfo> OrderInfo(string UserId)
        {

            var productOrders = Database.Products.GetAll().Join(Database.ProductOrders.GetAll(),
                p => p.Id,
                po => po.ProductId,
                (p, po) => new
                {
                    OrderId = po.OrderId,
                    ProductId = p.Id,
                    ProductName = p.ProductName
                });

            List<OrderInfo> userInfoList = new List<OrderInfo>();
            List<Order> orders = OrderOfUser(UserId).ToList();

            foreach (var item in orders)
            {
                var result = productOrders.Where(l => l.OrderId == item.Id).Select(l => l.ProductName).ToList();
                OrderInfo orderInfo = new OrderInfo() { OrderId = item.Id };
                orderInfo.ProductNames.AddRange(result);

                userInfoList.Add(orderInfo);
            }
            return userInfoList;
        }

        public IEnumerable<Order> OrderOfUser(string UserId)
        {
            return Database.Orders.FindByCondition(item => item.UserId == UserId);

        }
    }
}
